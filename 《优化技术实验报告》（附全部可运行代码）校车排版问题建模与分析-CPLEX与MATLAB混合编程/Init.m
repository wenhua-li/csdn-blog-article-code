function Population = Init(PopSize)
% 本函数用于构造初始解
global bus
pop.decs=[];
pop.obj=[];
pop.con=[];
pop.detail=[];
Population = repmat(pop,1,PopSize);

for i=1:PopSize
    Population(i).decs = randi([bus.minInterval,bus.maxInterval],1,bus.nT);
end

Population = CalObj(Population);
