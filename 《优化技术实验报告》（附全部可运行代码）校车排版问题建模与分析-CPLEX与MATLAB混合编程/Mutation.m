function X = Mutation(X)
% 本函数用于个体的变异
n = length(X);
PM = 1/n; %变异概率

for i=1:n
   if rand<PM
       X(i) = X(i)+randi([-5,5],1,1);
   end
end

end
