clear;clc;close all
%% 载入模型参数
LoadData();

%% 参数设置
PopSize = 50;
MaxGen = 100;
plt = 1; % 运行过程是否实时画迭代优化图，默认关闭(可极大提高运行速度)

%% 初始化
Population = Init(PopSize);
ConvergenceObj = zeros(2,PopSize);
ConvergenceCon = zeros(2,PopSize);

% return
%% 开始优化求解
h = figure();
for gen = 1:MaxGen
    MatingPool = randperm(PopSize,PopSize); %父代挑选
    Offspring = GA(Population(MatingPool)); %进行交叉变异操作
    Population = EnviornmentalSelection(Population,Offspring,gen/MaxGen); %挑选子代
    RecordInfo(); % 记录迭代优化信息
end
BestSol = BestInd(end);
disp(['发车时间间隔：' num2str(BestSol.decs)])
disp(['时段发车数量：' num2str(floor(60./BestSol.decs))])
disp(['乘客总时间成本:' num2str(BestSol.Q) ', 公交公司运行费用:' num2str(BestSol.G)])
disp(['总体载客率：' num2str(100*BestSol.zaikelv) '%'])
figure
yyaxis right
plot(BestSol.decs,'-s','linewidth',2)
ylabel('发车时间间隔/分钟')
hold on
yyaxis left
plot(bus.C,'-d','linewidth',2)
ylabel('客流量')
xlabel('时段')