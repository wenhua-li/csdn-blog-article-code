global bus

bus.nStation = 35; %站点数量
bus.nT = 17; %时段数量
bus.minInterval = 2;
bus.maxInterval = 30;
bus.alpha = 0.7;
bus.beta = 0.3;
bus.C = [103,188,274,586,501,624,491,660,437,412,597,586,554,708,688,350,175];%每个时间段的截面客流量
bus.s = 32;%线路总长
bus.m = 4;%每公里成本费用
bus.R =50;%车容量
bus.v = 1; %最小配车数
bus.y = 0.57; %薪资水平(元/分钟)
