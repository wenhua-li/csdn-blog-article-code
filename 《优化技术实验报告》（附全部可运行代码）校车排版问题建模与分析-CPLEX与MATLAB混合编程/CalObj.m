function Population = CalObj(Population)
% 本函数用于计算种群个体的目标函数值
N = length(Population);
global bus;
for i=1:N
   x = Population(i).decs;
   
   T = sum(bus.C .* x ./2); % 乘客的总等待时间
   Q = bus.y*T; % 乘客总时间成本
   n = floor(60./x); % 每个时间段的发车班次
   G = bus.s * bus.m * sum(n); % 公交公司运行费用
    
   %% 计算目标函数值
   zaikelv = sum(bus.C)/(bus.R*sum(n)); % 计算总的载客率
   f = bus.alpha * Q + bus.beta * G + 1000*abs(zaikelv-0.8);
   
   %% 计算约束违反情况
   old_con = 0;
   con = 0;
   detail = "";
   
   if zaikelv <= 0.4
       con = con + 1;
   end
   if con ~= old_con
       detail = detail+"载客率过低；";
       old_con = con;
   end
   
   if zaikelv>=1.2
       con = con + 1;
   end
   if con ~= old_con
       detail = detail+"载客率过高；";
       old_con = con;
   end
   
   %% 封装数据
   Population(i).obj = f;
   Population(i).con = con;
   Population(i).detail = detail;
   Population(i).Q = Q;
   Population(i).G = G;
   Population(i).zaikelv = zaikelv;
end