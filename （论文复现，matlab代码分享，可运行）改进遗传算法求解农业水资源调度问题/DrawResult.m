close all

figure;
plot(trace_obj)
% hold on
% plot(trace_con)
% legend('目标值','违反约束')
title('最优解目标值变化')
xlabel('迭代次数')
ylabel('目标值')

figure
bar(bestsol.q,0.4,'g')
% for i=1:11
%     text(i-0.2,bestsol.q(i)+0.2,num2str(bestsol.q(i)))
% end

hold on
boxplot([Qmax;Qmin],'PlotStyle','compact','MedianStyle','line')
% box_v = findall(gca,'Tag','Box');
% lg1=legend(box_v,{'设计流量'},'Location','SouthWest');
% ah=axes('position',get(gca,'position'),'visible','off');
% 
% lg2=legend(ah,'优化流量');
xlabel('渠系编号')
ylabel('下级渠道流量(m^3/s)')


figure;
for i=1:11
   line([bestsol.tstart(i),bestsol.tend(i)+1],[i,i],'LineWidth',4) 
   hold on
end

ylabel('渠系编号')
xlabel('配水时段(4h)')

figure
Qneed = [22,19,60,100,26,32,22,17,48,95,96].*1000; %子灌区需水量
plot(Qneed,'--o')
hold on
plot(bestsol.Q,'-*')
xlabel('渠系编号')
ylabel('渠道水量')
legend('计划水量','实际水量')
