function offspring=Mutate(population,state)
% 本函数完成变异操作
global T Qmax Qmin
N=length(population);

if state<0.5
    
    offspring1 = population;
    for i=1:N
        pop1 = population(i);
        pop2 = population(randperm(N,1));
        
        % 随机挑选1-2个位置进行交叉操作
        pos = randperm(11,randi([1,3],1));
        pop1.tstart(pos) =  pop2.tstart(pos);
        pop1.tend(pos) =  pop2.tend(pos);
        pop1.q(pos) =  pop2.q(pos);
        
        offspring1(i) = pop1;
    end
    
    
    offspring2 = population;
    for i=1:N
        pop1 = population(i);
        % 开始时间
        pos = randperm(11,randi([1,2],1));
        pop1.tstart(pos) = pop1.tstart(pos) + randi([-2,2],size(pos));
        pop1.tstart(pos) = max(pop1.tstart(pos),1);
        pop1.tstart(pos) = min(pop1.tstart(pos),T*24/4-1); %修复上下界
        pop1.tend(pos) = max(pop1.tstart(pos),pop1.tend(pos));
        
        offspring2(i) = pop1;
    end
    
    offspring3 = population;
    for i=1:N
        pop1 = population(i);
        
        % 结束时间
        pos = randperm(11,randi([1,2],1));
        pop1.tend(pos) = pop1.tend(pos) + randi([-2,2],size(pos));
        pop1.tend(pos) = max(pop1.tstart(pos),pop1.tend(pos));
        pop1.tend(pos) = min(pop1.tstart(pos),T*24/4); %修复上下界
        offspring3(i) = pop1;
    end
    
    offspring = CalObj([offspring1,offspring2,offspring3]);
else
    
    offspring4 = population;
    for i=1:N
        pop1 = population(i);
        
        % 供水量
        pos = randperm(11,randi([1,2],1));
        pop1.q(pos) = pop1.q(pos) + 0.2.*rands(1,length(pos));
        pop1.q = max(Qmin,pop1.q);
        pop1.q = min(Qmax,pop1.q); %修复上下界
        offspring4(i) = pop1;
    end
    offspring = CalObj(offspring4);
end

