function offspring=Mutate(population)
% 本函数完成变异操作

N=length(population);
Qmax = [0.8,0.6,1,1,0.6,0.6,0.5,0.5,0.6,0.8,1.5];
Qmin = 0.6.*Qmax;

offspring = population;
for i=1:N
    pop1 = population(i);
    pop2 = population(randperm(N,1));
    
    if rand < 0.5
        % 随机挑选1-2个位置进行交叉操作
        pos = randperm(11,randi([1,2],1));
        pop1.tstart(pos) =  pop2.tstart(pos);
        pop1.tend(pos) =  pop2.tend(pos);
        pop1.q(pos) =  pop2.q(pos);
    end

    % 选择概率进行编译操作
    if rand < 0.4
        % 开始时间
        pos = randperm(11,randi([1,2],1));
        pop1.tstart(pos) = pop1.tstart(pos) + randi([-2,2],size(pos));
        pop1.tstart(pos) = max(pop1.tstart(pos),1);
        pop1.tstart(pos) = min(pop1.tstart(pos),5*24/4-1); %修复上下界
    end
    
    if rand < 0.4
        % 结束时间
        pos = randperm(11,randi([1,2],1));
        pop1.tend(pos) = pop1.tend(pos) + randi([-2,2],size(pos));
        pop1.tend(pos) = max(pop1.tstart(pos),pop1.tend(pos));
        pop1.tend(pos) = min(pop1.tstart(pos),5*24/4); %修复上下界
    end
        
    if rand < 0.9
        % 供水量
        pos = randperm(11,randi([1,2],1));
        pop1.q(pos) = pop1.q(pos) + 0.2.*rands(1,length(pos));
        pop1.q = max(Qmin,pop1.q);
        pop1.q = min(Qmax,pop1.q); %修复上下界
    end
    offspring(i) = pop1;
end

offspring = CalObj(offspring);