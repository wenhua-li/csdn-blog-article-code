%% 清空环境
clc
clear
close all

%% 全局参数
global T Qmax Qmin
T = 3;
Qmax = [0.8,0.6,1,1,0.6,0.6,0.5,0.5,0.6,0.8,1.5];
Qmin = 0.6.*Qmax;

%% 遗传算法参数
maxgen=200;                         %进化代数
sizepop=300;                       %种群规模

%% 个体初始化
population = Init(sizepop);

trace_obj = zeros(1,maxgen);
trace_con = zeros(1,maxgen);

%% 进化开始
for i=1:maxgen
    % 交叉变异
    offspring = Mutate(population,i/maxgen);
    % 挑选新个体
    population = Select(population,offspring,sizepop);
    
    % 记录信息
    bestobj = population(1).obj;
    trace_obj(i) = bestobj;
    trace_con(i) = population(1).cons;
    
    if ~mod(i,10)
        cons = [population.cons];
        num = sum(cons==0);
        avgcons = mean(cons);
        disp(['第' num2str(i) '代，满足约束个体数量：' num2str(num), '，最佳个体：' num2str(bestobj)])
    end
end
%进化结束
bestsol = population(1);

DrawResult()
