function population=Select(population,offspring,N)
% 本函数对每一代种群中的染色体进行选择，以进行后面的交叉和变异

joint = [population,offspring];
objs = [joint.obj]';
cons = [joint.cons]';

[~,index] = sortrows([cons,objs]);

joint = joint(index);
objs = objs(index);

% 删除重复个体
del = [];
for i=1:length(joint)-1
    if find(i==del)
        continue;
    end
   for j=i+1:length(joint)
       if objs(i) == objs(j)
          del = [del j]; 
       end
   end
end
joint(del) = [];

population = joint(1:N);

% newpop = [];

% for i=1:N
%     
% end