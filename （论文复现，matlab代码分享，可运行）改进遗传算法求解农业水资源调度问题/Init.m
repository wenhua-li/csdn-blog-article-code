function population = Init(N)
global T Qmax Qmin

empty.tstart = [];
empty.tend = [];
empty.q = [];

population = repmat(empty,[1,N]);

for i=1:N
    tstart = randi([1,T*24/4-1],1,11); %供水开始时间
    tend = zeros(size(tstart)); %供水结束时间
    for j=1:11
        tend(j) = tstart(j)  + randi([1,(T*24/4-tstart(j) )],1);
    end
    q = Qmin + 0.5.*(Qmax-Qmin);
    
    population(i).tstart = tstart;
    population(i).tend = tend;
    population(i).q = q;
end

population = CalObj(population);