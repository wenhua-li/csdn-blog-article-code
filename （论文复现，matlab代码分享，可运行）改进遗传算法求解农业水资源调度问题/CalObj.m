function population = CalObj(population)
% population = Init(10);
global T
N = length(population);

beta = 0.5;
A = 3.4;
m = 0.5;
Jd = 0.4;
Ju = 1.3;
ad = 0.6;
au = 1.3;

L = [1.8,1.8,4.2,5.8,1.25,1.2,0.88,1.03,1.4,0.9,6.17]; %渠道长度
distance = [3,3.9,4.4,4.7,5,5.3,5.7,5.9,6.2,6.7,10.23]; %下级渠首到上级渠首
Qneed = [22,19,60,100,26,32,22,17,48,95,96].*1000; %子灌区需水量


for i=1:N
    tstart = population(i).tstart; % 解码
    tend = population(i).tend;
    tlast = tend - tstart + 1;
    q = population(i).q;
    
    % 是否处于供水状态
    isgongshui = zeros(T*24/4,11);
    for qu = 1:11
       isgongshui(tstart(qu):tend(qu),qu) = 1; 
    end
    
    % 下级渠道的配水水量
    Q = q.*tlast.*3600.*4;
    
    % 下级渠道的损失流量
    Q_down_loss = 0.01 .* A .* q.^(1-m) .* L .* 1000;
    
    % 用水户缺水量
    Ws = max(Qneed - Q, 0);
    
    % 计算各时间段渠首流量
    Q_t_shou = zeros(T*24/4,11);
    for t=1:T*24/4
        Q_t_shou(t,:) = q(11).*isgongshui(t,11);
        for qu = 10:-1:1
            Q_t_shou(t,qu) = Q_t_shou(t,qu+1) + q(qu)*isgongshui(t,qu);
        end
    end
    
    % 上级渠道的损失流量
    Q_up_loss = 0.01 .* A .* sum(Q_t_shou).^(1-m) .* distance .* 1000;
    
    %% 计算目标函数值
    Wl = sum(Q_down_loss + Q_up_loss);
    F = sum(Ws) + Wl;
    
    %% 检查约束条件
    cons = 0;
    % 水量约束
    if sum(Q) > 500000
       cons = cons + sum(Q)-500000;
    end
    
    % 渠道供水能力约束
    Q_ganqu_t = Q_t_shou(:,1);
    cons = cons + sum(max(Q_ganqu_t-2.5*Ju,0)) + sum(max(2.5*Jd-Q_ganqu_t,0));
    
    %% 封装
    population(i).obj = F;
    population(i).cons = cons;
    population(i).Q = Q;
    population(i).Wl = Wl;
    population(i).Ws = sum(Ws);
    population(i).Q_down_loss = Q_down_loss;
    population(i).Q_up_loss = Q_up_loss;
end