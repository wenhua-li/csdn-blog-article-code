
clc;
clear;
close all;

%% Problem Definition

CostFunction=@(x) myfun(x);      % Cost Function

nVar=24;             % 时间段

VarSize=[1 nVar];   % Size of Decision Variables Matrix

VarMin= 2451.5.*ones(1,24);        % 水位下界 %这里为了产生初始解做了相关调整
VarMax= 2455.*ones(1,24);          % 水位上界

% Number of Objective Functions
% nObj=numel(CostFunction(unifrnd(VarMin,VarMax,VarSize)));
nObj = 2;

%% NSGA-II Parameters

MaxIt=70;      % Maximum Number of Iterations

nPop=80;        % Population Size

pCrossover=0.7;                         % Crossover Percentage
nCrossover=2*round(pCrossover*nPop/2);  % Number of Parnets (Offsprings)

pMutation=0.4;                          % Mutation Percentage
nMutation=round(pMutation*nPop);        % Number of Mutants

mu=0.02;                    % Mutation Rate

sigma=0.1*(VarMax-VarMin);  % Mutation Step Size


%% Initialization

empty_individual.Position=[];
empty_individual.Cost=[];
empty_individual.Rank=[];
empty_individual.DominationSet=[];
empty_individual.DominatedCount=[];
empty_individual.CrowdingDistance=[];

pop=repmat(empty_individual,nPop,1);

disp('产生初始可行解...')
for i=1:nPop
    
    flag=0;
    while flag==0
        tmp=[];
        for j=1:1:nVar
            tmp = [tmp unifrnd(VarMin(j),VarMax(j),1)];
        end
        flag = test(tmp); % 检查约束 约束不满足就重新生成解
    end
    pop(i).Position=tmp;
    
    pop(i).Cost=CostFunction(pop(i).Position);
    
end

% pause

% Non-Dominated Sorting
[pop, F]=NonDominatedSorting(pop);

% Calculate Crowding Distance
pop=CalcCrowdingDistance(pop,F);

% Sort Population
[pop, F]=SortPopulation(pop);


%% NSGA-II Main Loop

for it=1:MaxIt
    
    % 交叉
    popc=repmat(empty_individual,nCrossover/2,2);
    for k=1:nCrossover/2
        
        i1=randi([1 nPop]);
        p1=pop(i1);
        
        i2=randi([1 nPop]);
        p2=pop(i2);
        
        [popc(k,1).Position, popc(k,2).Position]=Crossover(p1.Position,p2.Position,VarMin,VarMax);
        if test(popc(k,1).Position)+test(popc(k,2).Position)==2
            popc(k,1).Cost=CostFunction(popc(k,1).Position);
            popc(k,2).Cost=CostFunction(popc(k,2).Position);
        else
            popc(k,1)=p1;
            popc(k,2)=p2;
        end
        
    end
    popc=popc(:);
    
    % 变异
    popm=repmat(empty_individual,nMutation,1);
    for k=1:nMutation
        
        i=randi([1 nPop]);
        p=pop(i);
        
        popm(k).Position=Mutate(p.Position,mu,sigma,VarMin,VarMax);
        
        %% 越界处理
%         chrom=[popm(k).Position];
%         chrom(find(chrom<VarMin(1)))=VarMin(1);
%         chrom(find(chrom>VarMax(1)))=VarMax(1);
%         popm(k).Position=chrom;
        
        if test(popm(k).Position)
            
            popm(k).Cost=CostFunction(popm(k).Position);
        else
            popm(k)=p;
        end
        
    end
    
    % 合并父代和子代
    pop=[pop
        popc
        popm]; %#ok
    
    % Non-Dominated Sorting
    [pop, F]=NonDominatedSorting(pop);
    
    % Calculate Crowding Distance
    pop=CalcCrowdingDistance(pop,F);
    
    % Sort Population
    pop=SortPopulation(pop);
    
    % 截取前n个解
    pop=pop(1:nPop);
    
    % Non-Dominated Sorting
    [pop, F]=NonDominatedSorting(pop);
    
    % Calculate Crowding Distance
    pop=CalcCrowdingDistance(pop,F);
    
    % Sort Population
    [pop, F]=SortPopulation(pop);
    
    % Store F1
    F1=pop(F{1});
    
    % Show Iteration Information
    disp(['Iteration ' num2str(it) ': Number of F1 Members = ' num2str(numel(F1))]);
    
    % Plot F1 Costs
    figure(1);
    PlotCosts(F1);
    pause(0.01);
    
end

%% Results
result
