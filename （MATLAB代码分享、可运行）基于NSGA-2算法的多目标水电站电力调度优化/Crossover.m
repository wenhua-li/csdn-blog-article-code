%
% Copyright (c) 2015, Yarpiz (www.yarpiz.com)
% All rights reserved. Please read the "license.txt" for license terms.
%
% Project Code: YPEA120
% Project Title: Non-dominated Sorting Genetic Algorithm II (NSGA-II)
% Publisher: Yarpiz (www.yarpiz.com)
% 
% Developer: S. Mostapha Kalami Heris (Member of Yarpiz Team)
% 
% Contact Info: sm.kalami@gmail.com, info@yarpiz.com
%

function [y1, y2]=Crossover(x1,x2,vmin,vmax)

    alpha=rand(size(x1));
    
    y1=alpha.*x1+(1-alpha).*x2;
    y2=alpha.*x2+(1-alpha).*x1;
    
    for i=1:1:4
       if y1(i)>vmax(i)
           y1(i)=vmax(i);
       end
       if y1(i)<vmin(i)
           y1(i)=vmin(i);
       end
       
       if y2(i)>vmax(i)
           y2(i)=vmax(i);
       end
       if y2(i)<vmin(i)
           y2(i)=vmin(i);
       end
    end
    
end