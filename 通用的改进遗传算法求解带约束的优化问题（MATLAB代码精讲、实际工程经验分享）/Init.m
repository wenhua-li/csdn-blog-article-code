function Population = Init(PopSize)
% 本函数用于构造初始解
pop.decs=[];
pop.obj=[];
pop.con=[];
pop.detail=[];
Population = repmat(pop,1,PopSize);

for i=1:PopSize
    tmp = 10.*rands(2,1);
    Population(i).decs = tmp;
end

Population = CalObj(Population);
