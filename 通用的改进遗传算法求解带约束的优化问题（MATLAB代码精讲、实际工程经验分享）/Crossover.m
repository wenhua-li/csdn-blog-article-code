function [C1, C2] = Crossover(P1, P2)
% 本函数用于种群的交叉，总共设计两种交叉策略
n = length(P1);

pos = randperm(n,1); %这里每次交叉只挑选一个位置
C1 = P1; C2 = P2;

C1(pos) = P2(pos); C2(pos) = P1(pos);
end
