function Offspring = GA(Population)

N = length(Population);
Offspring = Population;
for i=1:floor(N/2)
    P1 = Population(2*i-1).decs;
    P2 = Population(2*i).decs;
    
    [C1,C2] = Crossover(P1,P2);
    C1 = Mutation(C1);
    C2 = Mutation(C2);
    
    Offspring(2*i-1).decs = C1;
    Offspring(2*i).decs = C2;
end

Offspring = CalObj(Offspring);