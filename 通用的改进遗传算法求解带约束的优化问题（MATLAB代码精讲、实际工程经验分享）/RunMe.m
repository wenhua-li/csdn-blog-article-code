clear;clc;close all

%% 参数设置
PopSize = 20;
MaxGen = 20;
plt = 1; % 运行过程是否实时画迭代优化图，默认关闭(可极大提高运行速度)

%% 初始化
Population = Init(PopSize);
ConvergenceObj = zeros(2,PopSize);
ConvergenceCon = zeros(2,PopSize);
BestSol = repmat(Population(1),1,MaxGen);

%% 开始优化求解
h = figure();
for gen = 1:MaxGen
    MatingPool = TournamentSelection(2,PopSize,[Population.con],[Population.obj]); %挑选父代
    Offspring = GA(Population(MatingPool)); %进行交叉变异操作
    Population = EnviornmentalSelection(Population,Offspring,gen/MaxGen); %挑选子代
    RecordInfo(); % 记录迭代优化信息
end
