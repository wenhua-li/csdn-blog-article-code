function l=Result(Chrom)
%% 计算各个体的路径长度
% 输入：
l=0;
load = [
    150
    100
    200
    100
    150
    150
    200
    200
    200
    100];

num_zhong = [120
50
100
70
100
110
150
160
160
70
];
num_kong = [30
50
100
30
50
40
50
140
40
30
];

for i=1:1:10
   
   if Chrom(i) < 4
       disp(['第' num2str(i) '条船，停靠港口：1,泊位：' num2str(Chrom(i))]);
   else
       disp(['第' num2str(i) '条船，停靠港口：2,泊位：' num2str(Chrom(i)-3)]);
   end
end

arrive_time = 6:1:15;
num_bowei = [3 3];%泊位数量
num_boat = 10; %穿的数量

%仿真求解适应度值
[NIND,num_boat]=size(Chrom);

wait_time = zeros(1,num_boat);
departure_time = zeros(1,num_boat);
service_time = zeros(1,sum(num_bowei));

c_wait = zeros(1,num_boat); %等待费用
c_sail = zeros(1,num_boat); %航行成本
c_transfer = zeros(1,num_boat); %转移成本


x=Chrom;
for j=1:1:num_boat
    if x(j) > num_bowei(1) % 选择第二个港口，到达时间要延迟
        arrive_time(j) = arrive_time(j) + 210/40;
        c_transfer(j) = (num_zhong(j) * 15 + num_kong(j) * 10) * 210;
        c_sail(j) = load(j) * 5 * 210;
    end
    if arrive_time(j) > service_time(x(j)) % 没有被占用,直接服务
        wait_time(j) = 0;
        departure_time(j) = arrive_time(j) + load(j)*15/60;
        service_time(x(j)) = departure_time(j);
    else % 已经被占用了
        wait_time(j) = service_time(x(j)) - arrive_time(j);
        departure_time(j) = arrive_time(j) + wait_time(j) + load(j)*15/60;
        service_time(x(j)) = departure_time(j);
    end
    
end
c_wait = wait_time .* 8000;
%len = sum(wait_time);
len = sum(c_wait) + sum(c_sail) + sum(c_transfer);


wait_time
departure_time
service_time
disp(['等待费用    航行费用  转移费用'])
disp([sum(c_wait)  sum(c_sail)  sum(c_transfer)])
