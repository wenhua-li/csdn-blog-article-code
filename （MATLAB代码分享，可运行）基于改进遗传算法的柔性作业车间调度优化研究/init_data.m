empty.num_gongxu=[];
empty.gongxu=[];

empty_gongxu.num_jichuang=[];
empty_gongxu.data=[];

info = repmat(empty,1,Global.I); %工件加工能耗/时间信息表

info(1).num_gongxu=3;
info(1).gongxu=repmat(empty_gongxu,1,info(1).num_gongxu);
info(1).gongxu(1).num_jichuang = 2;
info(1).gongxu(1).bianhao = [3,4];
info(1).gongxu(1).data=[3762132	589	68;3356324	815	78];
info(1).gongxu(2).num_jichuang = 2;
info(1).gongxu(2).bianhao = [5,6];
info(1).gongxu(2).data=[185473	185	81;208950	171	77];
info(1).gongxu(3).num_jichuang = 1;
info(1).gongxu(3).bianhao = 8;
info(1).gongxu(3).data=[489007	475	78];

info(2).num_gongxu=4;
info(2).gongxu=repmat(empty_gongxu,1,info(1).num_gongxu);
info(2).gongxu(1).num_jichuang = 4;
info(2).gongxu(1).bianhao = [1,2,3,4];
info(2).gongxu(1).data=[4431457 1532 103; 4387694 1768 64; 4922201 1178 78; 4417923 1501 91];
info(2).gongxu(2).num_jichuang = 2;
info(2).gongxu(2).bianhao = [5,7];
info(2).gongxu(2).data=[949708 261 76; 985683 234 87];
info(2).gongxu(3).num_jichuang =2;
info(2).gongxu(3).bianhao = [8,9];
info(2).gongxu(3).data=[786297 476 99; 972452 413 114];
info(2).gongxu(4).num_jichuang =1;
info(2).gongxu(4).bianhao = [11];
info(2).gongxu(4).data=[326605 415 37];

info(3).num_gongxu=3;
info(3).gongxu=repmat(empty_gongxu,1,info(1).num_gongxu);
info(3).gongxu(1).num_jichuang = 4;
info(3).gongxu(1).bianhao = [1,2,3,4];
info(3).gongxu(1).data=[2992184	717	46; 3174087	923	47; 3042597	745	53; 2975382	789 45];
info(3).gongxu(2).num_jichuang = 4;
info(3).gongxu(2).bianhao = [1,2,3,4];
info(3).gongxu(2).data=[452937	110	22; 440618	146	17; 446235	121	19; 426837	135	15];
info(3).gongxu(3).num_jichuang =2;
info(3).gongxu(3).bianhao = [6,7];
info(3).gongxu(3).data=[73826	67	39; 68819	58	43];

info(4).num_gongxu=4;
info(4).gongxu=repmat(empty_gongxu,1,info(1).num_gongxu);
info(4).gongxu(1).num_jichuang = 4;
info(4).gongxu(1).bianhao = [1,2,3,4];
info(4).gongxu(1).data=[523763	166	89; 458283	193	71; 500921	166	69; 473815	169	63];
info(4).gongxu(2).num_jichuang = 4;
info(4).gongxu(2).bianhao = [1,2,3,4];
info(4).gongxu(2).data=[661014	216	68; 651601	287	47; 656023	230	53; 614314	245	46];
info(4).gongxu(3).num_jichuang =1;
info(4).gongxu(3).bianhao = 11;
info(4).gongxu(3).data=[326605	415	33];
info(4).gongxu(4).num_jichuang =1;
info(4).gongxu(4).bianhao = 10;
info(4).gongxu(4).data=[401115	187	17];

info(5).num_gongxu=4;
info(5).gongxu=repmat(empty_gongxu,1,info(1).num_gongxu);
info(5).gongxu(1).num_jichuang = 3;
info(5).gongxu(1).bianhao = [1,2,3];
info(5).gongxu(1).data=[4431457	1532	49; 4387694	1768	56; 4922201	1178	51];
info(5).gongxu(2).num_jichuang = 2;
info(5).gongxu(2).bianhao = [5,7];
info(5).gongxu(2).data=[73826	43	43; 68819	59	57];
info(5).gongxu(3).num_jichuang =1;
info(5).gongxu(3).bianhao = 9;
info(5).gongxu(3).data=[786297	476	97];
info(5).gongxu(4).num_jichuang =1;
info(5).gongxu(4).bianhao = 11;
info(5).gongxu(4).data=[204288	168	46];