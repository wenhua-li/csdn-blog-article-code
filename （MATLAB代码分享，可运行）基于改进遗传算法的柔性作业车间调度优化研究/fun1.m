function [y,release_time,T_start,T_need,T_end,T_total,T_jiagong,T_duidao,T_kongxian,P_total,P_jiagong,P_duidao,P_kongxian] = fun1(x)
nVar = numel(x);
global Global info

%% 从头到尾安排工件进行加工
T_jiagong = 0;
T_duidao = 0;
T_kongxian = 0;
T_start = zeros(Global.I,4); %开工时间
T_need = zeros(Global.I,4); %加工时间
T_end = zeros(Global.I,4); %完工时间
release_time = zeros(1,Global.M); %下一次的可用时间

P_jiagong = 0;
P_duidao = 0;
P_kongxian = 0;

count_gongxu=ones(1,Global.I);
count_gongjian=zeros(1,Global.M); %记录机器使用次数
for i=1:nVar/2
    cur_gongjian = x(i);
    cur_gongxu = count_gongxu(cur_gongjian);
    count_gongxu(cur_gongjian) = count_gongxu(cur_gongjian)+1;
    cur_machine = x(nVar/2+i);
    cur_machine_code = info(cur_gongjian).gongxu(cur_gongxu).bianhao(cur_machine);
    count_gongjian(cur_machine_code) = count_gongjian(cur_machine_code) + 1;
    
    if cur_gongxu==1 %当前工件的首个工序
        if release_time(cur_machine_code)>Global.Ai(cur_gongjian) %当前几床被占用
            T_start(cur_gongjian,cur_gongxu) = release_time(cur_machine_code);
            T_need(cur_gongjian,cur_gongxu) = Global.Ni(cur_gongjian)*sum(info(cur_gongjian).gongxu(cur_gongxu).data(cur_machine,2:3));
            release_time(cur_machine_code) = release_time(cur_machine_code) + T_need(cur_gongjian,cur_gongxu);
        else %当前机床没有被占用
            T_start(cur_gongjian,cur_gongxu) = Global.Ai(cur_gongjian);
            T_need(cur_gongjian,cur_gongxu) = Global.Ni(cur_gongjian)*sum(info(cur_gongjian).gongxu(cur_gongxu).data(cur_machine,2:3));
            P_kongxian = P_kongxian + (T_start(cur_gongjian,cur_gongxu)-release_time(cur_machine_code))*Global.m_power(cur_machine,1);
            T_kongxian = T_kongxian+(T_start(cur_gongjian,cur_gongxu)-release_time(cur_machine_code));
            release_time(cur_machine_code) = T_start(cur_gongjian,cur_gongxu) + T_need(cur_gongjian,cur_gongxu);
        end
        T_jiagong = T_jiagong+Global.Ni(cur_gongjian)*sum(info(cur_gongjian).gongxu(cur_gongxu).data(cur_machine,2));
        T_duidao = T_duidao+Global.Ni(cur_gongjian)*sum(info(cur_gongjian).gongxu(cur_gongxu).data(cur_machine,3));
        P_jiagong = P_jiagong + info(cur_gongjian).gongxu(cur_gongxu).data(cur_machine,1)*Global.Ni(cur_gongjian);
        P_duidao = P_duidao + info(cur_gongjian).gongxu(cur_gongxu).data(cur_machine,3)* ...
            Global.m_power(cur_machine_code,2)*Global.Ni(cur_gongjian);
        T_end(cur_gongjian,cur_gongxu) = T_start(cur_gongjian,cur_gongxu) + T_need(cur_gongjian,cur_gongxu);
        
        if cur_machine_code==Global.fault(1) && ...
                T_start(cur_gongjian,cur_gongxu)<Global.fault(2)&&T_end(cur_gongjian,cur_gongxu)>Global.fault(2) %发生了故障
            release_time(cur_machine_code) = 100000000;%设置成一个非常大的数
        end
        
    else %不是首个工序
        if release_time(cur_machine_code)>T_end(cur_gongjian,cur_gongxu-1) %当前几床被占用
            T_start(cur_gongjian,cur_gongxu) = release_time(cur_machine_code);
            T_need(cur_gongjian,cur_gongxu) = Global.Ni(cur_gongjian)*sum(info(cur_gongjian).gongxu(cur_gongxu).data(cur_machine,2));
            release_time(cur_machine_code) = release_time(cur_machine_code) + T_need(cur_gongjian,cur_gongxu);
        else
            T_start(cur_gongjian,cur_gongxu) = T_end(cur_gongjian,cur_gongxu-1);
            T_need(cur_gongjian,cur_gongxu) = Global.Ni(cur_gongjian)*sum(info(cur_gongjian).gongxu(cur_gongxu).data(cur_machine,2));
            P_kongxian = P_kongxian + (T_start(cur_gongjian,cur_gongxu)-release_time(cur_machine_code))*Global.m_power(cur_machine,1);
            T_kongxian = T_kongxian+(T_start(cur_gongjian,cur_gongxu)-release_time(cur_machine_code));
            release_time(cur_machine_code) = T_start(cur_gongjian,cur_gongxu) + T_need(cur_gongjian,cur_gongxu);
        end
        P_jiagong = P_jiagong + info(cur_gongjian).gongxu(cur_gongxu).data(cur_machine,1)*Global.Ni(cur_gongjian);
        T_end(cur_gongjian,cur_gongxu) = T_start(cur_gongjian,cur_gongxu) + T_need(cur_gongjian,cur_gongxu);
        T_jiagong = T_jiagong+T_need(cur_gongjian,cur_gongxu);
        
        if cur_machine_code==Global.fault(1) && ...
                T_start(cur_gongjian,cur_gongxu)<Global.fault(2)&&T_end(cur_gongjian,cur_gongxu)>Global.fault(2) %发生了故障
            release_time(cur_machine_code) = 100000000;%设置成一个非常大的数
        end
        if cur_machine_code==Global.fault(1) && T_start(cur_gongjian,cur_gongxu)>Global.fault(2)
            release_time(cur_machine_code) = 100000000;%设置成一个非常大的数
        end
    end
end

non_use = Global.M-numel(find(count_gongjian)==0);

T_total = max(release_time);
P_total = P_jiagong + P_duidao + P_kongxian;
xishu = Global.xishu;
y = sum([T_total P_total].*xishu) + non_use*1000000000;