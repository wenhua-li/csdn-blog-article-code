function ret=Cross(pcross,chrom,sizepop)
%本函数完成交叉操作
% pcorss                input  : 交叉概率
% lenchrom              input  : 染色体的长度
% chrom                 input  : 染色体群
% sizepop               input  : 种群规模
% ret                   output : 交叉后的染色体
global Global info

nVar = size(chrom,2)/2;
for i=1:sizepop
    
    % 交叉概率决定是否进行交叉
    pick=rand;
    while pick==0
        pick=rand;
    end
    if pick>pcross
        continue;
    end
    
    % 随机选择染色体进行自交叉
    index=ceil(rand.*sizepop);
    tmp_chrom = chrom(index,:);
    
    pos = randperm(nVar,2);
    tmp = tmp_chrom(pos(1));
    tmp_chrom(pos(1)) = tmp_chrom(pos(2));
    tmp_chrom(pos(2)) = tmp;
    
    cur_gongxu=ones(1,Global.I); %重新分配工序
    for i=1:nVar
        cur_gongjian=tmp_chrom(i);
        num_machine = info(cur_gongjian).gongxu(cur_gongxu(cur_gongjian)).num_jichuang;
        if cur_gongjian==tmp_chrom(pos(1)) || cur_gongjian==tmp_chrom(pos(2))
            tmp_chrom(nVar+i) = randperm(num_machine,1);
        end
        cur_gongxu(cur_gongjian) = cur_gongxu(cur_gongjian)+1;
    end
    
    chrom(index,:) = tmp_chrom;
end
ret=chrom;
