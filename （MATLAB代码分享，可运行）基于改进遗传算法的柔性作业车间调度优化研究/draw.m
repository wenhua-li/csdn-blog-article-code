x=bestchrom;
nVar = numel(x);
global Global info

[y,release_time,T_start,T_need,T_end,T_total,T_jiagong,T_duidao,T_kongxian,P_total,P_jiagong,P_duidao,P_kongxian] = Global.fun(x);

%% 画甘特图
color=['r','g','y','c','m','b'];
count_gongxu=ones(1,Global.I);
for i =1:nVar/2
    cur_gongjian = bestchrom(i);
    cur_gongxu = count_gongxu(cur_gongjian);
    count_gongxu(cur_gongjian) = count_gongxu(cur_gongjian)+1;
    cur_machine = bestchrom(nVar/2+i);
    cur_machine_code = info(cur_gongjian).gongxu(cur_gongxu).bianhao(cur_machine);
    
    rec(1) = T_start(cur_gongjian,cur_gongxu);%矩形的横坐标
    rec(2) = cur_machine_code-0.5;  %矩形的纵坐标
    rec(3) = T_need(cur_gongjian,cur_gongxu);  %矩形的x轴方向的长度
    rec(4) = 1;
    txt=sprintf('(%d-%d)',cur_gongjian,cur_gongxu);%将机器号，工序号，加工时间连城字符串
%     txt=sprintf('(%d-%d-%d)',cur_gongjian,Global.Ni(cur_gongjian),cur_gongxu);%将机器号，工序号，加工时间连城字符串
    rectangle('Position',rec,'LineWidth',0.5,'LineStyle','-','FaceColor',color(cur_gongjian));%draw every rectangle
    text(T_start(cur_gongjian,cur_gongxu),cur_machine_code,txt,'FontWeight','Bold','FontSize',8);%label the id of every task  ，字体的坐标和其它特性
end
    
yticks([0:1:12])
title('甘特图')
grid on
box on
hold off
xlabel('时间/秒')
ylabel('机床序号')