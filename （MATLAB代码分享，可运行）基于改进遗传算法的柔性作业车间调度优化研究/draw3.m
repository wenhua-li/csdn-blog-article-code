x=bestchrom;
nVar = numel(x);
global Global info

[y,release_time,T_start,T_need,T_end,T_total,T_jiagong,T_duidao,T_kongxian,P_total,P_jiagong,P_duidao,P_kongxian] = Global.fun(x);

Global.fault = [2 80000]; %第一个是机器编号，第二个是故障时间
figure(2)
hold off

%% 画甘特图
color=['r','g','y','c','m','b'];
count_gongxu=ones(1,Global.I);
remain_task=[];
fault_gongjian = zeros(1,Global.I);
for i =1:nVar/2
    cur_gongjian = bestchrom(i);
    cur_gongxu = count_gongxu(cur_gongjian);
    count_gongxu(cur_gongjian) = count_gongxu(cur_gongjian)+1;
    cur_machine = bestchrom(nVar/2+i);
    cur_machine_code = info(cur_gongjian).gongxu(cur_gongxu).bianhao(cur_machine);
    
    if fault_gongjian(cur_gongjian) == 1
        T_start_new = T_start(cur_gongjian,cur_gongxu);
        T_remain = T_need(cur_gongjian,cur_gongxu);
        remain_task=[remain_task;[cur_gongxu,cur_gongjian,T_start_new,T_remain,cur_machine_code]];
        continue
    end
    
    if cur_machine_code==Global.fault(1) && ...
            T_start(cur_gongjian,cur_gongxu)<Global.fault(2)&&T_start(cur_gongjian,cur_gongxu)+T_need(cur_gongjian,cur_gongxu)>Global.fault(2) %在工序进行时，发生了故障
        T_start_new = Global.fault(2);
        T_remain = T_need(cur_gongjian,cur_gongxu)-(Global.fault(2)-T_start(cur_gongjian,cur_gongxu));
        T_need(cur_gongjian,cur_gongxu) = Global.fault(2)-T_start(cur_gongjian,cur_gongxu);
        remain_task=[remain_task;[cur_gongxu,cur_gongjian,T_start_new,T_remain,0]];
        fault_gongjian(cur_gongjian) = 1;
    elseif cur_machine_code==Global.fault(1) &&  T_start(cur_gongjian,cur_gongxu)>Global.fault(2)%在工序未开始之前，发生了故障
        T_start_new = T_start(cur_gongjian,cur_gongxu);
        T_remain = T_need(cur_gongjian,cur_gongxu);
        remain_task=[remain_task;[cur_gongxu,cur_gongjian,T_start_new,T_remain,0]];
        fault_gongjian(cur_gongjian) = 1;
        continue
    end
    
    
    rec(1) = T_start(cur_gongjian,cur_gongxu);%矩形的横坐标
    rec(2) = cur_machine_code-0.5;  %矩形的纵坐标
    rec(3) = T_need(cur_gongjian,cur_gongxu);  %矩形的x轴方向的长度
    rec(4) = 1;
    txt=sprintf('(%d-%d-%d)',cur_gongjian,Global.Ni(cur_gongjian),cur_gongxu);%将机器号，工序号，加工时间连城字符串
    rectangle('Position',rec,'LineWidth',0.5,'LineStyle','-','FaceColor',color(cur_gongjian));%draw every rectangle
    text(T_start(cur_gongjian,cur_gongxu),cur_machine_code,txt,'FontWeight','Bold','FontSize',8);%label the id of every task  ，字体的坐标和其它特性
end

%% 安排剩下的任务
new_remain_task=[];
for i=1:size(remain_task,1)
    cur_gongjian =remain_task(i,2);
    cur_gongxu =remain_task(i,1);
    T_s = remain_task(i,3);
    T_n = remain_task(i,4);
    machine_code = remain_task(i,5);
    
    if machine_code==0 %处在故障机床上面
        can_machine = info(cur_gongjian).gongxu(cur_gongxu).bianhao;
        can_machine(find(can_machine==Global.fault(1)))=[];
        tmp_release = release_time(can_machine);
        [~,so] = sort(tmp_release,'ascend'); %找到最快能用的机床
        sel_machine = can_machine(so(1));
        if tmp_release(so(1))<T_s
            s=T_s;
        else
            s=tmp_release(so(1));
        end
        new_remain_task=[new_remain_task;[cur_gongxu,cur_gongjian,s,T_n,sel_machine]];
        T_end(cur_gongjian,cur_gongxu) = s+T_n;
        release_time(sel_machine) = s+T_n;
    else %不在故障机床上面
        if T_end(cur_gongjian,cur_gongxu-1)>T_s
            T_s = T_end(cur_gongjian,cur_gongxu-1);
            T_end(cur_gongjian,cur_gongxu) = T_s+T_n;
        end
        new_remain_task=[new_remain_task;[cur_gongxu,cur_gongjian,T_s,T_n,machine_code]];
    end
    
    %画图
    rec(1) = new_remain_task(i,3);%矩形的横坐标
    rec(2) = new_remain_task(i,5)-0.5;  %矩形的纵坐标
    rec(3) = new_remain_task(i,4);  %矩形的x轴方向的长度
    rec(4) = 1;
    txt=sprintf('(%d-%d-%d)',new_remain_task(i,2),Global.Ni(new_remain_task(i,2)),new_remain_task(i,1));%将机器号，工序号，加工时间连城字符串
    rectangle('Position',rec,'LineWidth',0.5,'LineStyle','-','FaceColor',color(new_remain_task(i,2)));%draw every rectangle
    text(new_remain_task(i,3),new_remain_task(i,5),txt,'FontWeight','Bold','FontSize',8);%label the id of every task  ，字体的坐标和其它特性
end

yticks([0:1:12])
title('有故障')
grid on
hold off