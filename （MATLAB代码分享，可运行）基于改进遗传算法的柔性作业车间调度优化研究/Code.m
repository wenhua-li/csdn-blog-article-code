function ret=Code()
%本函数将变量编码成染色体，用于随机初始化一个种群
% ret        output: 染色体的编码值
global Global info

all_gongxu=[];
for i=1:Global.I
    all_gongxu=[all_gongxu i.*ones(1,info(i).num_gongxu)];
end

index=randperm(numel(all_gongxu));
ret = all_gongxu(index);

%根据安排的工序安排机器
machine=[];
cur_gongxu=ones(1,Global.I);
for i=1:numel(ret)
   cur_gongjian=ret(i);
   num_machine = info(ret(i)).gongxu(cur_gongxu(cur_gongjian)).num_jichuang;
   machine=[machine randperm(num_machine,1)];
   cur_gongxu(cur_gongjian) = cur_gongxu(cur_gongjian)+1;
end

ret = [ret machine];