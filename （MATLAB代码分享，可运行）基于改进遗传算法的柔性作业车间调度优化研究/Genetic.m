%% 清空环境
clc
clear
close all

%% 模型参数
load data
global Global info
Global.M = 11;
Global.I = 5;
Global.m_power = M_power;

% Ai=zeros(5,1);
% Ni=ones(5,1);

Global.Ni = Ni;
Global.Ai = Ai;
Global.Di = Di;
% Global.fault = [4 50000]; %第一个是机器编号，第二个是故障时间

Global.Ni(5)=130;
Global.xishu=[0,1];
Global.fun = @(x) fun(x);%改成fun1就是有故障的。
init_data();

nVar = 0;
for i=1:Global.I
    nVar = nVar+info(i).num_gongxu;
end

%% 遗传算法参数
maxgen=100;                         %进化代数
sizepop=100;                       %种群规模
k1=7e+8;                      %交叉概率
k2=4e+8;                  %变异概率

%% 个体初始化
individuals=struct('fitness',zeros(1,sizepop), 'chrom',[]);  %种群结构体
avgfitness=[];                                               %种群平均适应度
bestfitness=[];                                              %种群最佳适应度
bestchrom=[];                                                %适应度最好染色体
% 初始化种群
for i=1:sizepop
    individuals.chrom(i,:)=Code();       %随机产生个体
    x=individuals.chrom(i,:);
    individuals.fitness(i)=Global.fun(x);                     %个体适应度
end

%找最好的染色体
[bestfitness bestindex]=min(individuals.fitness);
bestchrom=individuals.chrom(bestindex,:);  %最好的染色体
avgfitness=sum(individuals.fitness)/sizepop; %染色体的平均适应度
% 记录每一代进化中最好的适应度和平均适应度
trace=[];

%% 进化开始
h = waitbar(0,'正在优化中。。。');
for i=1:maxgen
    waitbar(i/maxgen,h);
    %自适应修改交叉变异概率
    pcross=k1/(avgfitness-bestfitness);                      %交叉概率
    pmutation=k2/(avgfitness-bestfitness);                  %变异概率
    
    % 选择操作
    individuals=Select(individuals,sizepop);
    avgfitness=sum(individuals.fitness)/sizepop;
    % 交叉操作
    individuals.chrom=Cross(pcross,individuals.chrom,sizepop);
    % 变异操作
    individuals.chrom=Mutation(pmutation,individuals.chrom,sizepop);
    
    % 计算适应度
    for j=1:sizepop
        x=individuals.chrom(j,:);
        individuals.fitness(j)=Global.fun(x);
    end
    
    %找到最小和最大适应度的染色体及它们在种群中的位置
    [newbestfitness,newbestindex]=min(individuals.fitness);
    [worestfitness,worestindex]=max(individuals.fitness);
    % 代替上一次进化中最好的染色体
    if bestfitness>newbestfitness
        bestfitness=newbestfitness;
        bestchrom=individuals.chrom(newbestindex,:);
        
        figure(1)
        hold off
        plot(1,1)
        draw
        pause(0.01)
    end
    individuals.chrom(worestindex,:)=bestchrom;
    individuals.fitness(worestindex)=bestfitness;
    
    avgfitness=sum(individuals.fitness)/sizepop;
    trace=[trace;avgfitness bestfitness]; %记录每一代进化中最好的适应度和平均适应度
end
close(h);
%进化结束

%% 结果显示
[r c]=size(trace);
figure
plot([1:r]',trace(:,2),'r-','LineWidth',2);
title(['函数值曲线  ' '终止代数＝' num2str(maxgen)],'fontsize',12);
xlabel('进化代数','fontsize',12);ylabel('函数值','fontsize',12);
legend('迭代优化曲线')
disp(['函数值:' num2str(bestfitness)]);

