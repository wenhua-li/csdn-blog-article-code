function ret=Mutation(pmutation,chrom,sizepop)
% 本函数完成变异操作
% pcorss                input  : 变异概率
% lenchrom              input  : 染色体长度
% chrom                 input  : 染色体群
% sizepop               input  : 种群规模
% pop                   input  : 当前种群的进化代数和最大的进化代数信息
% ret                   output : 变异后的染色体

global Global info

nVar = size(chrom,2)/2;
for i=1:sizepop
    
    % 交叉概率决定是否进行变异
    pick=rand;
    while pick==0
        pick=rand;
    end
    if pick>pmutation
        continue;
    end
    
    % 随机选择染色体进行自交叉
    index=ceil(rand.*sizepop);
    tmp_chrom = chrom(index,:);
    
    pos = randperm(nVar,randperm(3,1));
    
    cur_gongxu=ones(1,Global.I); %小红心分配工序
    for i=1:nVar
        cur_gongjian=tmp_chrom(i);
        num_machine = info(cur_gongjian).gongxu(cur_gongxu(cur_gongjian)).num_jichuang;
        if find(tmp_chrom(pos)==cur_gongjian)
            tmp_chrom(nVar+i) = randperm(num_machine,1);
        end
        cur_gongxu(cur_gongjian) = cur_gongxu(cur_gongjian)+1;
    end
    
    chrom(index,:) = tmp_chrom;
end
ret=chrom;