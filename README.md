# CSDN博客论文代码

#### 介绍
运筹不帷幄的CSDN博客论文代码
https://blog.csdn.net/c__batcoder

**如果本项目对您有任何帮助，麻烦关注博客，点赞收藏评论~**


#### 目前已经开源的代码
通用的改进遗传算法求解带约束的优化问题（MATLAB代码精讲、实际工程经验分享）
https://blog.csdn.net/c__batcoder/article/details/120373651

（论文复现，matlab代码分享，可运行）改进遗传算法求解农业水资源调度问题
https://blog.csdn.net/c__batcoder/article/details/122312160

(matlab代码分享，可运行) 多技能员工排班调度多目标优化（技能熟练度包含学习型、遗忘型）（Part 1）
https://blog.csdn.net/c__batcoder/article/details/122333619

（MATLAB代码分享，可运行）基于改进遗传算法的柔性作业车间调度优化研究
https://blog.csdn.net/c__batcoder/article/details/122311620

Matlab代码及解析（可运行） 基于改进遗传算法求解带时间窗约束多卫星任务规划
https://blog.csdn.net/c__batcoder/article/details/120201863

【MATLAB代码】基于模拟退化算法的船舶调度优化问题、船舶进港排班
https://blog.csdn.net/c__batcoder/article/details/123585473

《优化技术实验报告》（附全部可运行代码）校车排版问题建模与分析-CPLEX与MATLAB混合编程
https://blog.csdn.net/c__batcoder/article/details/122306475

（MATLAB代码分享、可运行）基于NSGA-2算法的多目标水电站电力调度优化
https://blog.csdn.net/c__batcoder/article/details/122311285

部分非开源代码获取方式见博客内容