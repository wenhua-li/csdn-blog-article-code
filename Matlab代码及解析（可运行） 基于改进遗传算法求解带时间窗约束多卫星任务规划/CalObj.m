function population = CalObj(population)
% population = Init(1);

global Global

N = length(population);

for i=1:N
    ind = population(i);
    satellite_list = ind.decs(1:Global.num_satellite);
    ground_list = ind.decs(Global.num_satellite+1:end);
    
    ground_next_release_time = zeros(1,Global.num_ground); %地面站的下一次可观测时间
    
    time_start_guance = zeros(1,Global.num_satellite); %开始观测时间
    time_end_guance = zeros(1,Global.num_satellite); %结束观测时间
    index_window_guance = zeros(1,Global.num_satellite); %观测窗口的编号
    cons = 0; %违反约束的情况
    
    for j = 1:Global.num_satellite
        cur_satellite = satellite_list(j); %当前卫星
        cur_ground = ground_list(j); %当前卫星选择的地面站
        
        % 依次检查当前卫星的观测时间窗
        flag = 0;
        for m = 1:Global.num_visible_window(cur_satellite,cur_ground)
            time_start = Global.visible_window{cur_satellite,cur_ground}(2*m-1); %观测窗口的开始时间
            time_end = Global.visible_window{cur_satellite,cur_ground}(2*m); %观测窗口的结束时间
            if ground_next_release_time(cur_ground) > time_end
                continue;
            end
            time_begin = max(ground_next_release_time(cur_ground),time_start);
            
            if time_begin < time_end - Global.sat_need_time(cur_satellite) %当前观测窗口可以用来观测
                time_start_guance(cur_satellite) = time_begin;
                time_end_guance(cur_satellite) = time_start_guance(cur_satellite) + Global.sat_need_time(cur_satellite);
                ground_next_release_time(cur_ground) = time_end_guance(cur_satellite) + 60; %设备的反应时间
                index_window_guance(cur_satellite) = m;
                flag=1;
                break;
            end
        end
        if flag == 0 %说明这个卫星没有被安排到地面站
            cons = cons + Global.sat_need_time(cur_satellite);
        end
    end
    
    %% 计算目标函数
    T = max(time_end_guance);
    total_rank = 0;
    for j = 1:Global.num_satellite
        cur_satellite = satellite_list(j); %当前卫星
        cur_ground = ground_list(j); %当前卫星选择的地面站
        total_rank = total_rank + Global.rank_ground(cur_ground) * Global.rank_satellite(cur_satellite);
    end
    
    %% 传出值
    population(i).objs = [T - 10*total_rank];
    population(i).cons = cons;
    population(i).time_start_guance = time_start_guance;
    population(i).time_end_guance = time_end_guance;
    population(i).ground_list = ground_list;
    population(i).index_window_guance = index_window_guance;
    population(i).ground_next_release_time = ground_next_release_time;
end