function population = Init(N)

global Global
empty.decs = [];
empty.objs = [];
empty.cons = [];

population = repmat(empty,1,N);
for i=1:N
    population(i).decs = [randperm(Global.num_satellite,Global.num_satellite) ...
        randi([1,Global.num_ground],1,Global.num_satellite)];
end
population = CalObj(population);