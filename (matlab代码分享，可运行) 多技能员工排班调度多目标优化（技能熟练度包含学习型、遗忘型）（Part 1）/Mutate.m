%
% Copyright (c) 2015, Yarpiz (www.yarpiz.com)
% All rights reserved. Please read the "license.txt" for license terms.
%
% Project Code: YPEA120
% Project Title: Non-dominated Sorting Genetic Algorithm II (NSGA-II)
% Publisher: Yarpiz (www.yarpiz.com)
%
% Developer: S. Mostapha Kalami Heris (Member of Yarpiz Team)
%
% Contact Info: sm.kalami@gmail.com, info@yarpiz.com
%

function y=Mutate(x,mu,sigma)
global m Eij Rjd
nVar=numel(x);
y=x;

index = 1;
for i=1:1:m
    %找到需要的技能
    skill = find(Rjd(:,i)==1);
    for j=1:1:numel(skill)
        %找到相关人员
        member = find(Eij(:,skill(j))>0);
        %随机挑一个工人去搞事情
        pick = member(ceil(numel(member)*rand(1)));
        if rand < 0.2
            y(index) = pick;
        end
        index = index + 1;
    end
end

%% 寻找变异位置
% index = ceil(nVar.*rand(1,n_mu));
% v = x(index);
% x(index) = ceil(40.*rand(1,n_mu));
% flag = test(x);
% if ~flag
%     x(index) = v;
% end

%     nMu=ceil(mu*nVar);
%
%     j=randsample(nVar,nMu);
%     if numel(sigma)>1
%         sigma = sigma(j);
%     end
%
%     y=x;
%
%     y(j)=x(j)+sigma.*randn(size(j));

end