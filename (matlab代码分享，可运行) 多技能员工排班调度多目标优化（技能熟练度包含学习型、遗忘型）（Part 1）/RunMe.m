%
% Copyright (c) 2015, Yarpiz (www.yarpiz.com)
% All rights reserved. Please read the "license.txt" for license terms.
%
% Project Code: YPEA120
% Project Title: Non-dominated Sorting Genetic Algorithm II (NSGA-II)
% Publisher: Yarpiz (www.yarpiz.com)
% 
% Developer: S. Mostapha Kalami Heris (Member of Yarpiz Team)
% 
% Contact Info: sm.kalami@gmail.com, info@yarpiz.com
%

clc;
clear;
close all;

%% 模型参数表示
global m s n Tjd_min salary Rjd needskill Eij Tijd nVar
m = 10;
s = 8;
n = 40;

Tjd_min=[1	2	4	0	11	0	2	0
1	2	2	0	5	0	1	0
0	2	0	0	8	1	2	0
2	4	2	0	3	0	6	0
0	4	0	6	0	2	0	1
0	0	2	0	2	0	2	4
0	0	1	0	1	0	5	1
0	0	4	0	4	0	1	1
0	0	5	0	2	0	1	1
0	0	3	0	4	0	3	1];

needskill=cell(m,2);

for i=1:1:m
    needskill{i,1} = find(Tjd_min(i,:)~=0);
    needskill{i,2} = numel(needskill{i,1});
end

[x,y]=find(Tjd_min>=1);
Rjd = zeros(size(Tjd_min));
for i=1:1:numel(x)
    Rjd(x(i),y(i)) = 1;
end
Rjd=Rjd';

salary=[8000	6000	10000	12000	7000	15000	10000	7000	9000	8000
7000	8000	11000	12000	7000	10000	13000	9000	10000	9000
10000	7000	9000	9000	8000	15000	12000	12000	9000	8000
9000	10000	12000	10000	8000	8000	10000	13000	9000	10000];
num_worker = numel(salary);
salary = reshape(salary,[1,numel(salary)]);

%从文件读取数据
[a,b,c]=xlsread('data.xlsx');
Eij=zeros(40,8);
for i=1:1:8
    str1=b{i,1};str2=b{i,2};
    tmp1=regexp(str1, ',', 'split');
    tmp2=regexp(str2, ',', 'split');
    for j=1:1:size(tmp1,2)
        x=str2num(tmp1{j});
        y=str2num(tmp2{j});
        Eij(x,i) = y;
    end
end

%计算Tijd
Tjd_min=Tjd_min';
Tijd = zeros(40,s,m);
for i=1:1:40
    for j=1:1:s
        for d=1:1:m
           Tijd(i,j,d) = Tjd_min(j,d)/Eij(i,j); 
        end
    end
end
% Tijd = ceil(Tijd);

%% Problem Definition

CostFunction=@(x) model3(x,Eij,Tijd);      % Cost Function

nVar=0;             % Number of Decision Variables
for i=1:1:m
   nVar = nVar + needskill{i,2}; 
end

VarSize=[1 nVar];   % Size of Decision Variables Matrix

VarMin=0;          % Lower Bound of Variables
VarMax=1;          % Upper Bound of Variables

% Number of Objective Functions
nObj=numel(CostFunction(ones(1,nVar)));


%% NSGA-II Parameters

MaxIt=400;      % Maximum Number of Iterations

nPop=50;        % Population Size

pCrossover=0.7;                         % Crossover Percentage
nCrossover=2*round(pCrossover*nPop/2);  % Number of Parnets (Offsprings)

pMutation=0.3;                          % Mutation Percentage
nMutation=round(pMutation*nPop);        % Number of Mutants

mu=0.02;                    % Mutation Rate

sigma=0.1*(VarMax-VarMin);  % Mutation Step Size


%% Initialization

empty_individual.Position=[];
empty_individual.Cost=[];
empty_individual.Rank=[];
empty_individual.DominationSet=[];
empty_individual.DominatedCount=[];
empty_individual.CrowdingDistance=[];

pop=repmat(empty_individual,nPop,1);

for i=1:nPop
    
    pop(i).Position=Code();%unifrnd(VarMin,VarMax,VarSize);
    
    pop(i).Cost=CostFunction(pop(i).Position);
    
end

% Non-Dominated Sorting
[pop, F]=NonDominatedSorting(pop);  

% Calculate Crowding Distance
pop=CalcCrowdingDistance(pop,F);

% Sort Population
[pop, F]=SortPopulation(pop);


%% NSGA-II Main Loop

for it=1:MaxIt
    
    % Crossover
    popc=repmat(empty_individual,nCrossover/2,2);
    for k=1:nCrossover/2
        
        i1=randi([1 nPop]);
        p1=pop(i1);
        
        i2=randi([1 nPop]);
        p2=pop(i2);
        
        [popc(k,1).Position, popc(k,2).Position]=Crossover(p1.Position,p2.Position);
        
        popc(k,1).Cost=CostFunction(popc(k,1).Position);
        popc(k,2).Cost=CostFunction(popc(k,2).Position);
        
    end
    popc=popc(:);
    
    % Mutation
    popm=repmat(empty_individual,nMutation,1);
    for k=1:nMutation
        
        i=randi([1 nPop]);
        p=pop(i);
        
        popm(k).Position=Mutate(p.Position,mu,sigma);
        
        popm(k).Cost=CostFunction(popm(k).Position);
        
    end
    
    % Merge
    pop=[pop
         popc
         popm]; %#ok
     
    % Non-Dominated Sorting
    [pop, F]=NonDominatedSorting(pop);

    % Calculate Crowding Distance
    pop=CalcCrowdingDistance(pop,F);

    % Sort Population
    pop=SortPopulation(pop);
    
    % Truncate
    pop=pop(1:nPop);
    
    % Non-Dominated Sorting
    [pop, F]=NonDominatedSorting(pop);

    % Calculate Crowding Distance
    pop=CalcCrowdingDistance(pop,F);

    % Sort Population
    [pop, F]=SortPopulation(pop);
    
    % Store F1
    F1=pop(F{1});
    
    % Show Iteration Information
    disp(['进化代数' num2str(it) ': 最优解个数 = ' num2str(numel(F1))]);
    
    % Plot F1 Costs
    figure(1);
    PlotCosts(F1);
    pause(0.01);
    
end

%% Results

