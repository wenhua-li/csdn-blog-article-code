%从文件读取数据
[a,b,c]=xlsread('data.xlsx');
Eij=zeros(40,8);
for i=1:1:8
    str1=b{i,1};str2=b{i,2};
    tmp1=regexp(str1, ',', 'split');
    tmp2=regexp(str2, ',', 'split');
    for j=1:1:size(tmp1,2)
        x=str2num(tmp1{j});
        y=str2num(tmp2{j});
        Eij(x,i) = y;
    end
end
%计算Tijd
Tijd = zeros(40,s,m);
for i=1:1:40
    for j=1:1:s
        for d=1:1:m
           Tijd(i,j,d) = Tjd_min(j,d)/Eij(i,j); 
        end
    end
end
x = [F1(2).Position];
x=x(1:43);

%% 解码
global m Rjd salary Tjd_min

% 按照任务顺序分配任务，直到完成
FTd = zeros(1,m); %任务完成时间
STd = zeros(1,m); %任务的开始时间
FTpd = zeros(1,m); %前序工作的完成时间
Td = zeros(1,m); %任务d的工期
total_salary = 0;
next_time_work = zeros(1,40); %辅助变量

a=-log(0.91)/log(2);
b=-log(1-0.04)/log(2);

%基本想法，每次分配完任务，更新Tijd表。
index = 1;
e_index = 1;
record = [];
for i=1:1:m
    %看一下前序工作的完成时间
    if i==3
        STd(i) = FTd(2);
    end
    if i==5
        STd(i) = max(FTd(1),FTd(2));
    end
    if i==6
        STd(i) = max(FTd(1),FTd(4));
    end
    if i==7
        STd(i) = max(FTd(3),FTd(5));
    end
    if i==8
        STd(i) = FTd(3);
    end
    if i==9
        STd(i) = max(FTd(6),max(FTd(7),FTd(8)));
    end
    if i==10
        STd(i) = FTd(9);
    end
    
    %找到需要的技能
    skill = find(Rjd(:,i)==1);
    needtime = zeros(1,numel(skill));
    for j=1:1:numel(skill)
        %找到相关人员
        worker = x(index);
        total_salary = total_salary + salary(worker);
        needtime(j) = Tijd(worker,skill(j),i);
        %检查工人什么时候可以开工
        if STd(i) > next_time_work(worker) %任务开始时，工人处于空闲
            %buganshenme
        else %任务开始时，工人处于忙碌状态
            needtime(j) = needtime(j) + next_time_work(worker) - STd(i);
        end
        next_time_work(worker) = STd(i) + needtime(j);
        record(index,:) = [worker i skill(j) STd(i) next_time_work(worker)];
        index = index+1;
    end
    Td(i) = max(needtime); %工期是最大的工人工作时间
    %分配完一个任务之后，更新Eij
    for j=1:1:numel(skill)
        %找到相关人员
        worker = x(e_index);
        Eij(worker,skill(j)) = Eij(worker,skill(j)) * (needtime(j)^a) * (Td(i)-needtime(j))^b;
        %更新T表
        Tijd(worker,skill(j),i) = Tjd_min(skill(j),i)/Eij(worker,skill(j)); 
        e_index = e_index+1;
    end
    
    FTd(i) = STd(i) + Td(i);
end

final_date = max(FTd);

z=[final_date;total_salary];

figure
color=['r','g','y','c','m','b','k','r','g','c',];
for i=1:m
    rec(1) = STd(i);%矩形的横坐标
    rec(2) = i-0.5;  %矩形的纵坐标
    rec(3) = FTd(i)-STd(i);  %矩形的x轴方向的长度
    rec(4) = 1;
    rectangle('Position',rec,'LineWidth',0.5,'LineStyle','-','FaceColor',color(i));%draw every rectangle
end
