function [ ret ] = Code(  )
% 编码
global nVar Rjd m Eij

index=1;
ret = zeros(1,nVar);
for i=1:1:m
    %找到需要的技能
    skill = find(Rjd(:,i)==1);
    for j=1:1:numel(skill)
       %找到相关人员
       member = find(Eij(:,skill(j))>0);
       %随机挑一个工人去搞事情
       pick = member(ceil(numel(member)*rand(1)));
       ret(index) = pick;
       index = index + 1;
    end
end

