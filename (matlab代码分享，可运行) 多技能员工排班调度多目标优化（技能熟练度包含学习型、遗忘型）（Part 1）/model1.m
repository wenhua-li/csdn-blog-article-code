function [ z ] = fun( x,Eij,Tijd)
%% 解码
global m Rjd salary

% 按照任务顺序分配任务，直到完成
FTd = zeros(1,m); %任务完成时间
STd = zeros(1,m); %任务的开始时间
FTpd = zeros(1,m); %前序工作的完成时间
Td = zeros(1,m); %任务d的工期
total_salary = 0;
next_time_work = zeros(1,40); %辅助变量

%基本想法，每次分配完任务，更新Tijd表。
index = 1;
for i=1:1:m
    %看一下前序工作的完成时间
    if i==3
        STd(i) = FTd(2);
    end
    if i==5
        STd(i) = max(FTd(1),FTd(2));
    end
    if i==6
        STd(i) = max(FTd(1),FTd(4));
    end
    if i==7
        STd(i) = max(FTd(3),FTd(5));
    end
    if i==8
        STd(i) = FTd(3);
    end
    if i==9
        STd(i) = max(FTd(6),max(FTd(7),FTd(8)));
    end
    if i==10
        STd(i) = FTd(9);
    end
    
    %找到需要的技能
    skill = find(Rjd(:,i)==1);
    needtime = zeros(1,numel(skill));
    for j=1:1:numel(skill)
        %找到相关人员
        worker = x(index);
        total_salary = total_salary + salary(worker);% .* Eij(worker,skill(j));
        needtime(j) = Tijd(worker,skill(j),i);
        %检查工人什么时候可以开工
        if STd(i) > next_time_work(worker) %任务开始时，工人处于空闲
            %buganshenme
        else %任务开始时，工人处于忙碌状态
            needtime(j) = needtime(j) + next_time_work(worker) - STd(i);
        end
        next_time_work(worker) = STd(i) + needtime(j);
        index = index+1;
    end
    Td(i) = max(needtime); %工期是最大的工人工作时间
    FTd(i) = STd(i) + Td(i);
end

final_date = max(FTd);

z=[final_date;total_salary];
end

