%
% Copyright (c) 2015, Yarpiz (www.yarpiz.com)
% All rights reserved. Please read the "license.txt" for license terms.
%
% Project Code: YPEA120
% Project Title: Non-dominated Sorting Genetic Algorithm II (NSGA-II)
% Publisher: Yarpiz (www.yarpiz.com)
% 
% Developer: S. Mostapha Kalami Heris (Member of Yarpiz Team)
% 
% Contact Info: sm.kalami@gmail.com, info@yarpiz.com
%

function [y1, y2]=Crossover(x1,x2)

    nVar = numel(x1);
    alpha=rand(size(x1));
    %% �������
    n_c=4;
    index = ceil(nVar.*rand(1,n_c));
    v1=x1(index);
    v2=x2(index);
    y1=x1;y1(index)=v2;
    y2=x2;y2(index)=v1;
    
%     y1=alpha.*x1+(1-alpha).*x2;
%     y2=alpha.*x2+(1-alpha).*x1;
    
end